package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.model.IWBS;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @NotNull
    private Date created = new Date();

    public Task(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final Status status,
            @Nullable final String projectId
    ) {
        super(userId);
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return name.equals(task.name) &&
                Objects.equals(description, task.description) &&
                status == task.status &&
                Objects.equals(projectId, task.projectId) &&
                created.equals(task.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, projectId, created);
    }

}
