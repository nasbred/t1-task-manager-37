package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.user.EmailEmptyException;
import ru.t1.kharitonova.tm.exception.user.LoginEmptyException;
import ru.t1.kharitonova.tm.exception.user.PasswordEmptyException;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private List<User> userList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setLocked(true);
        userTest.setRole(Role.USUAL);

        userList.add(userAdmin);
        if(!userService.isLoginExist("admin")){
            userService.add(userAdmin);
        }
        userList.add(userTest);
        if(!userService.isLoginExist("test")){
            userService.add(userTest);
        }
    }

    @After
    public void clear() {
        userService.removeAll();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final User user = new User();
        user.setLogin("test_add");
        user.setPasswordHash("test_add");
        user.setEmail("test_add@test.ru");
        user.setRole(Role.USUAL);
        userService.add(user);
        Assert.assertEquals(userList.size() + 1, userService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<User> users = new ArrayList<>();
        final int count = userList.size();
        for (int i = 0; i < count; i++) {
            @NotNull final User user = new User();
            user.setLogin("test_add_all " + i);
            user.setPasswordHash("test_add_all " + i);
            user.setEmail("test_add@test.ru");
            user.setRole(Role.USUAL);
            users.add(user);
        }
        userService.addAll(users);
        Assert.assertEquals(count * 2, userService.getSize());
    }

    @Test
    public void testCreate() {
        @NotNull final String login = "test_create";
        @NotNull final String password = "test_create";
        @NotNull final Role role = Role.USUAL;

        Assert.assertNotNull(userService.create(login, password, role));
        Assert.assertEquals(userList.size() + 1, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateWithNullLogin() {
        userService.create(null, "password");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateWithNullPassword() {
        userService.create("login", null);
    }

    @Test
    public void testFindAll() {
        @NotNull List<User> userList = userService.findAll();
        Assert.assertEquals(userService.getSize(), userList.size());
    }

    @Test
    public void testFindOneById() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final User actualUser = userService.findOneById(id);
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(user, actualUser);
        }
        Assert.assertNull(userService.findOneById("otherId"));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            Assert.assertNotNull(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(user, actualUser);
        }
        Assert.assertNull(userService.findByLogin("other_login"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindOneByEmail() {
        @NotNull final User user = userService.findByLogin("test");
        @Nullable final String email = user.getEmail();
        Assert.assertNotNull(email);
        @Nullable final User actualUser = userService.findByEmail(email);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getId(), actualUser.getId());

        Assert.assertNull(userService.findByEmail("other_email"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
    }

    @Test
    public void testGetSize() {
        final int userSize = userService.getSize();
        Assert.assertEquals(userList.size(), userSize);
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < userList.size(); i++) {
            @NotNull final User user = userList.get(i);
            userService.remove(user);
            Assert.assertEquals(userService.getSize(), userList.size() - i - 1);
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            Assert.assertNotNull(userService.removeOneById(id));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.removeOneById(id));
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            Assert.assertNotNull(userService.removeByLogin(login));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByLoginNegative() {
        @NotNull final String login = "Other_Login";
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.removeOneById(login));
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            Assert.assertNotNull(userService.removeByEmail(email));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        @NotNull final String email = "Other_Email";
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.removeByEmail(email));
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testSetPassword() {
        @NotNull final String newPassword = "newPassword";
        for (@NotNull final User user : userList) {
            @NotNull final IPropertyService propertyService = new PropertyService();
            @NotNull final String id = user.getId();
            @Nullable final String password = user.getPasswordHash();
            @NotNull final User actualUser = userService.setPassword(id, newPassword);
            Assert.assertNotEquals(password, actualUser.getPasswordHash());
            Assert.assertEquals(HashUtil.salt(propertyService, newPassword), actualUser.getPasswordHash());

            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, "")
            );
            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, null)
            );
        }

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword(null, newPassword)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword("", newPassword)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.setPassword("other_Id", newPassword)
        );
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "Update First Name";
        @NotNull final String lastName = "Update Last Name";
        @NotNull final String middleName = "Update Middle Name";
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @NotNull final User actualUser = userService.updateUser(id, firstName, lastName, middleName);
            Assert.assertEquals(firstName, actualUser.getFirstName());
            Assert.assertEquals(lastName, actualUser.getLastName());
            Assert.assertEquals(middleName, actualUser.getMiddleName());
        }
        Assert.assertEquals(userList.size(), userService.getSize());

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser(null, firstName, lastName, middleName)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser("", firstName, lastName, middleName)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.updateUser("otherId", firstName, lastName, middleName)
        );
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUser = userService.lockUserByLogin(login);
            Assert.assertNotNull(actualUser);
            Assert.assertTrue(actualUser.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("random_login"));
    }

    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUser = userService.unlockUserByLogin(login);
            Assert.assertNotNull(actualUser);
            Assert.assertFalse(actualUser.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("otherLogin"));
    }

}
