package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.ISessionService;
import ru.t1.kharitonova.tm.api.service.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.SessionRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Ignore
public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final List<Session> sessionList = new ArrayList<>();

    @NotNull
    private List<User> userList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        userList.add(userAdmin);
        if(!userService.isLoginExist("admin")){
            userService.add(userAdmin);
        }
        userList.add(userTest);
        if(!userService.isLoginExist("test")){
            userService.add(userTest);
        }

        @Nullable final String adminUserId = userAdmin.getId();
        @Nullable final String testUserId = userTest.getId();
        @NotNull final Session sessionAdmin = new Session(adminUserId, Role.ADMIN);
        @NotNull final Session sessionTest = new Session(testUserId, Role.USUAL);

        sessionList.add(sessionAdmin);
        sessionList.add(sessionTest);
        sessionService.addAll(sessionList);
    }

    @After
    public void clear() {
        sessionService.removeAll();
        sessionList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        final int expectedSize = sessionList.size();
        Assert.assertEquals(expectedSize, sessionService.getSize());
        @NotNull final Session session = new Session(userList.get(1).getId(), Role.USUAL);
        sessionService.add(session);
        Assert.assertEquals(expectedSize + 1, sessionService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull final List<Session> sessions = new ArrayList<>();
        final int size = sessionList.size();
        for (int i = 0; i < size; i++) {
            sessions.add(new Session(userList.get(1).getId(), Role.USUAL));
        }
        sessionService.addAll(sessions);
        Assert.assertEquals(size *2, sessionService.getSize());
    }

    @Test
    public void testAddForUser() {
        final int expectedSize = sessionList.size();
        int i = 0;
        Assert.assertEquals(expectedSize, sessionService.getSize());
        for (@NotNull final User user : userList) {
            @NotNull final Session session = new Session();
            Assert.assertNotNull(sessionService.add(user.getId(), session));
            i++;
            Assert.assertThrows(
                    UserIdEmptyException.class,
                    () -> sessionService.add(null, session)
            );
            Assert.assertThrows(
                    UserIdEmptyException.class,
                    () -> sessionService.add("", session)
            );
        }
        Assert.assertEquals(expectedSize + i, sessionService.getSize());
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            Assert.assertTrue(sessionService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(sessionService.existsById("Other_Id"));
        Assert.assertFalse(sessionService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionList.size(), sessions.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Session> actualSessions = sessionService.findAll(userId);
            @NotNull final List<Session> expectedSessions = sessionList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (@NotNull final Session expectedSession : expectedSessions) {
                Assert.assertEquals(expectedSession.getId(), expectedSessions.get(0).getId());
            }
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            Assert.assertEquals(id, sessionService.findOneById(id).getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final Session session = sessionService.findOneById(id);
        Assert.assertNull(session);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            @Nullable final String userId = session.getUserId();
            Assert.assertEquals(id, sessionService.findOneById(userId, id).getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Session session = sessionService.findOneById(userId, id);
        Assert.assertNull(session);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneByIndexNegative() {
        final int index = sessionList.size() + 1;
        Assert.assertNull(sessionService.findOneByIndex(index));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = sessionList.size() + 1;
        Assert.assertNull(sessionService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int projectSize = sessionService.getSize();
        Assert.assertEquals(sessionList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualSessionSize = sessionService.getSize(userId);
            @NotNull final List<Session> expectedProjects = sessionList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedSessionSize = expectedProjects.size();
            Assert.assertEquals(expectedSessionSize, actualSessionSize);
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < sessionList.size(); i++) {
            @NotNull final Session session = sessionList.get(i);
            sessionService.removeOne(session.getUserId(), session);
            Assert.assertEquals(sessionService.getSize(), sessionList.size() - i - 1);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String id = session.getId();
            Assert.assertNotNull(sessionService.removeOneById(id));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertNull(sessionService.removeOneById(id));
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testRemoveAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            sessionService.removeAllByUserId(userId);
            Assert.assertEquals(0, sessionService.getSize(userId));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdNegative() {
        sessionService.removeAllByUserId("Other_user_id");
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

}
