package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Ignore
public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        @NotNull final String adminId = userAdmin.getId();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        @NotNull final String userId = userTest.getId();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        if(!userService.isLoginExist("admin")){
            userService.add(userAdmin);
            userList.add(userAdmin);
        } else {
            userList.add(userService.findByLogin("admin"));
        }
        if(!userService.isLoginExist("test")){
            userService.add(userTest);
            userList.add(userTest);
        } else {
            userList.add(userService.findByLogin("test"));
        }

        projectList.add(new Project(adminId, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(adminId, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1).getId()));
        taskList.add(new Task(userId, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2).getId()));
        taskList.add(new Task(userId, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        projectService.removeAll();
        taskService.removeAll();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project(
                userList.get(0).getId(),
                "Test Project",
                "Test Add Project",
                Status.NOT_STARTED);
        projectService.add(project);
        Assert.assertTrue(projectService.existsById(project.getId()));

        projectService.remove(project);
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projects = new ArrayList<>();
        final int projectSize = projectService.getSize();
        for (int i = 0; i < projectSize; i++) {
            projects.add(new Project(
                    userList.get(0).getId(),
                    "Project Test Add " + i,
                    "Project Add Desc " + i,
                    Status.NOT_STARTED));
        }
        projectService.addAll(projects);
        Assert.assertEquals(projectSize * 2, projectService.getSize());

        projectService.removeAll(projects);
    }

    @Test
    public void testCreate() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Test_Project_" + user.getLogin();
            @NotNull final String description = "Test_Description_" + user.getLogin();
            @NotNull final Project project = projectService.create(userId, name, description);
            Assert.assertNotNull(project);
            projectService.remove(project);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(projectService.create(
                null,
                "Test Project",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(projectService.create(
                userList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        @Nullable List<Project> projects = projectService.findAll();
        if (projects == null) {
            projectService.addAll(projectList);
            projects = projectList;
        }
        for (@NotNull final Project project : projects) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
            @Nullable final Project actualProject = projectService.findOneById(userId, id);
            Assert.assertNotNull(actualProject);
            @NotNull final Status actualStatus = actualProject.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            for (int i = 0; i < userProjects.size(); i++) {
                @Nullable final Project project = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(project);
                projectService.changeProjectStatusByIndex(userId, i, Status.IN_PROGRESS);
                @NotNull final Status status = project.getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            projectService.changeProjectStatusByIndex(userId, userProjects.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertTrue(projectService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(projectService.existsById("Other_Id"));
        Assert.assertFalse(projectService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            Assert.assertNotNull(userId);
            @NotNull final List<Project> actualProjects = projectService.findAll(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(expectedProjects, actualProjects);
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertEquals(project, projectService.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final Project project = projectService.findOneById(id);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final String userId = project.getUserId();
            Assert.assertEquals(project, projectService.findOneById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Project project = projectService.findOneById(userId, id);
        Assert.assertNull(project);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneByIndexNegative() {
        final int index = projectService.getSize() + 1;
        projectService.findOneByIndex(index);
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualProject);
                @Nullable final Project expectedProject = projects.get(i);
                Assert.assertEquals(expectedProject, actualProject);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = projectList.size() + 1;
        Assert.assertNull(projectService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int projectSize = projectService.getSize();
        Assert.assertEquals(projectList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualProjectSize = projectService.getSize(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedProjectSize = expectedProjects.size();
            Assert.assertEquals(expectedProjectSize, actualProjectSize);
        }
    }

    @Test
    public void testUpdateByIdPositive() {
        @Nullable List<Project> projects = projectService.findAll();
        if (projects == null) {
            projectService.addAll(projectList);
            projects = projectList;
        }
        for (@NotNull final Project project : projects) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            @NotNull final String name = "Project Update";
            @NotNull final String description = "Description Update";
            @Nullable final Project actualProject = projectService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualProject.getName());
            Assert.assertEquals(description, actualProject.getDescription());

            projectService.remove(project);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = projectList.get(0).getId();
        @NotNull final String name = "Project Test Update " + id;
        @NotNull final String description = "Description Test Update " + id;
        Assert.assertNotNull(projectService.updateById(userId, id, name, description));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                @NotNull final String name = "Project Test Update " + i;
                @NotNull final String description = "Description Test Update " + i;
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.updateByIndex(userId, i, name, description);
                Assert.assertEquals(name, actualProject.getName());
                Assert.assertEquals(description, actualProject.getDescription());
                Assert.assertEquals(projects.get(i), actualProject);
            }
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < projectService.getSize(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(i);
            Assert.assertNotNull(project);
            projectService.removeOne(project.getUserId(), project);
            Assert.assertNull(projectService.findOneById(project.getId()));
            projectService.add(project);
        }
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertNotNull(projectService.removeOneById(id));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertNull(projectService.removeOneById(id));
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testRemoveAll() {
        @Nullable List<Project> projectsBcp = projectService.findAll();
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
        if (projectsBcp != null) projectService.addAll(projectsBcp);
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable List<Project> projectsBcp = projectService.findAll(userId);
            projectService.removeAllByUserId(userId);
            Assert.assertEquals(0, projectService.getSize(userId));
            projectService.addAll(projectsBcp);
        }
    }

}
