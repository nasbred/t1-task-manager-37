package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Ignore
public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @NotNull
    final String userId = UUID.randomUUID().toString();

    @NotNull
    final String projectName = "Project Name Test";

    @NotNull
    final String taskName = "Task Name Test";

    @NotNull
    final String description = "Description";

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        @NotNull final String adminId = userAdmin.getId();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        @NotNull final String userId = userTest.getId();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        if(!userService.isLoginExist("admin")){
            userService.add(userAdmin);
            userList.add(userAdmin);
        } else {
            userList.add(userService.findByLogin("admin"));
        }
        if(!userService.isLoginExist("test")){
            userService.add(userTest);
            userList.add(userTest);
        } else {
            userList.add(userService.findByLogin("test"));
        }

        projectList.add(new Project(adminId, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(adminId, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1).getId()));
        taskList.add(new Task(userId, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2).getId()));
        taskList.add(new Task(userId, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        projectService.removeAll();
        taskService.removeAll();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final String projectId = project.getId();
        @NotNull final Task task = taskService.create(userId, taskName, description);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, UUID.randomUUID().toString())
        );
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        Assert.assertEquals(taskService.findOneById(taskId).getProjectId(), projectId);
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final String projectId = project.getId();
        @NotNull final Task task = taskService.create(userId, taskName, description);
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertNull(projectService.findOneById(projectId));
        Assert.assertNull(taskService.findOneById(taskId));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", projectId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById(null, projectId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(userId, null)
        );
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final Project project = projectService.create(userId, projectName, description);
        @NotNull final String projectId = project.getId();
        @NotNull final Task task = taskService.create(userId, taskName, description);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, UUID.randomUUID().toString())
        );
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertEquals(taskService.findOneById(taskId).getProjectId(), projectId);
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertNull(task.getProjectId());
    }

}
