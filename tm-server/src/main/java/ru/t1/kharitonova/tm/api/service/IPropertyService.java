package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.component.ISaltProvider;
import ru.t1.kharitonova.tm.api.property.IDatabaseProperty;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
