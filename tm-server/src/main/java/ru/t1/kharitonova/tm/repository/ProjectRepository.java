package ru.t1.kharitonova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

    @NotNull
    private static final String TABLE_PROJECT = "tm_project";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    private static final String COLUMN_DESCRIPTION = "description";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_PROJECT;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(COLUMN_ID));
        project.setName(row.getString(COLUMN_NAME));
        project.setDescription(row.getString(COLUMN_DESCRIPTION));
        project.setUserId(row.getString(COLUMN_USER_ID));
        project.setStatus(Status.toStatus(row.getString(COLUMN_STATUS)));
        project.setCreated(row.getTimestamp(COLUMN_CREATED));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format("INSERT INTO %s " +
                        "(%s, %s, %s, %s, %s, %s)" +
                        " VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(),
                COLUMN_ID, COLUMN_NAME, COLUMN_DESCRIPTION,
                COLUMN_USER_ID, COLUMN_STATUS, COLUMN_CREATED
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        statement.executeUpdate();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) return null;
        if (userId == null || userId.isEmpty()) return null;
        project.setUserId(userId);
        @Nullable final Project result = add(project);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project model) {
        @NotNull final String sql = String.format("UPDATE %s " +
                        "SET %s = ?, %s = ?, %s = ?, %s = ? " +
                        "WHERE ID = ?;",
                getTableName(),
                COLUMN_NAME, COLUMN_DESCRIPTION,
                COLUMN_USER_ID, COLUMN_STATUS
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, model.getName());
        statement.setString(2, model.getDescription());
        statement.setString(3, model.getUserId());
        statement.setString(4, model.getStatus().toString());
        statement.setString(5, model.getId());
        statement.executeUpdate();
        statement.close();
        return model;
    }

    @Override
    public void removeAll(@NotNull List<Project> models) {
        super.removeAll();
    }

}
