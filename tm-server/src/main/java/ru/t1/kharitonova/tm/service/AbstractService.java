package ru.t1.kharitonova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IService;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final M result;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.add(model);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> addAll(@NotNull final Collection<M> models) {
        @Nullable final Collection<M> result;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.addAll(models);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final Connection connection = connectionService.getConnection();
        @Nullable final Collection<M> result;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            result = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @Nullable final M entity;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M record;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            record = repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable M record;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            record = repository.removeOneByIndex(index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final List<M> models) {
        if (models == null || models.isEmpty()) return;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll(models);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M update(@NotNull final M model) {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final M entity;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

}
