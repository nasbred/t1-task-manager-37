package ru.t1.kharitonova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_TASK = "tm_task";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    private static final String COLUMN_DESCRIPTION = "description";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    @NotNull
    private static final String COLUMN_PROJECT_ID = "project_id";

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_TASK;
    }

    @Override
    @SneakyThrows
    protected @NotNull Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString(COLUMN_ID));
        task.setName(row.getString(COLUMN_NAME));
        task.setDescription(row.getString(COLUMN_DESCRIPTION));
        task.setUserId(row.getString(COLUMN_USER_ID));
        task.setProjectId(row.getString(COLUMN_PROJECT_ID));
        task.setStatus(Status.toStatus(row.getString(COLUMN_STATUS)));
        task.setCreated(row.getTimestamp(COLUMN_CREATED));
        return task;
    }

    @Override
    @SneakyThrows
    public @NotNull Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format("INSERT INTO %s" +
                        " (%s, %s, %s, %s, %s, %s, %s)" +
                        " VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(), COLUMN_ID, COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_USER_ID,
                COLUMN_STATUS, COLUMN_CREATED, COLUMN_PROJECT_ID
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
        statement.setString(7, task.getProjectId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

    @Nullable
    @Override
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) return null;
        if (userId == null || userId.isEmpty()) return null;
        task.setUserId(userId);
        @Nullable final Task result = add(task);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), COLUMN_PROJECT_ID, COLUMN_USER_ID
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void removeAll(@NotNull final List<Task> models) {
        super.removeAll();
    }

    @Override
    @SneakyThrows
    public @Nullable Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE ID = ?",
                getTableName(), COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_USER_ID,
                COLUMN_STATUS, COLUMN_PROJECT_ID
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setString(3, task.getUserId());
        statement.setString(4, task.getStatus().toString());
        statement.setString(5, task.getProjectId());
        statement.setString(6, task.getId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

}
