package ru.t1.kharitonova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

public class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

    @NotNull
    private static final String TABLE_SESSION = "tm_session";

    @NotNull
    private static final String COLUMN_ROLE = "role";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_SESSION;
    }

    @Override
    @SneakyThrows
    protected @NotNull Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString(COLUMN_ID));
        session.setDate(row.getTimestamp(COLUMN_CREATED));
        session.setRole(Role.toRole(row.getString(COLUMN_ROLE)));
        session.setUserId(row.getString(COLUMN_USER_ID));
        return session;
    }

    @Override
    @SneakyThrows
    public @NotNull Session add(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) " +
                        "VALUES (?, ?, ?, ?);",
                getTableName(), COLUMN_ID, COLUMN_CREATED, COLUMN_ROLE, COLUMN_USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getRole() ==null? null: session.getRole().getDisplayName());
            statement.setString(4, session.getUserId());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    public void removeAll(@NotNull final List<Session> models) {
        super.removeAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE ID = ?",
                getTableName(), COLUMN_CREATED, COLUMN_ROLE, COLUMN_USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getRole().toString());
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }


}
