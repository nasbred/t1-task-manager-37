package ru.t1.kharitonova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String TABLE_USER = "tm_user";

    @NotNull
    private static final String COLUMN_LOGIN = "login";

    @NotNull
    private static final String COLUMN_PASSWORD = "password_hash";

    @NotNull
    private static final String COLUMN_EMAIL = "email";

    @NotNull
    private static final String COLUMN_FST_NAME = "first_name";

    @NotNull
    private static final String COLUMN_LAST_NAME = "last_name";

    @NotNull
    private static final String COLUMN_MID_NAME = "middle_name";

    @NotNull
    private static final String COLUMN_LOCKED = "locked";

    @NotNull
    private static final String COLUMN_ROLE = "role";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_USER;
    }

    @Override
    @SneakyThrows
    protected @NotNull User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(COLUMN_ID));
        user.setLogin(row.getString(COLUMN_LOGIN));
        user.setPasswordHash(row.getString(COLUMN_PASSWORD));
        user.setEmail(row.getString(COLUMN_EMAIL));
        user.setFirstName(row.getString(COLUMN_FST_NAME));
        user.setFirstName(row.getString(COLUMN_LAST_NAME));
        user.setFirstName(row.getString(COLUMN_MID_NAME));
        user.setLocked(row.getBoolean(COLUMN_LOCKED));
        user.setRole(Role.valueOf(row.getString(COLUMN_ROLE)));
        return user;
    }

    @NotNull
    @Override
    public User add(@NotNull User user) {
        @NotNull final String sql = String.format("INSERT INTO %s" +
                        " (%s, %s, %s, %s, %s, %s, %s, %s, %s)" +
                        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), COLUMN_ID, COLUMN_LOGIN, COLUMN_PASSWORD, COLUMN_EMAIL, COLUMN_FST_NAME,
                COLUMN_LAST_NAME, COLUMN_MID_NAME, COLUMN_LOCKED, COLUMN_ROLE
                );
        try {
            @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setBoolean(8, user.isLocked());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void removeAll(@NotNull List<User> models) {
        super.removeAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format("UPDATE %s" +
                        " SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE ID = ?",
                getTableName(), COLUMN_LOGIN, COLUMN_PASSWORD, COLUMN_EMAIL,
                COLUMN_FST_NAME, COLUMN_LAST_NAME, COLUMN_MID_NAME, COLUMN_LOCKED, COLUMN_ROLE
        );
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getFirstName());
        statement.setString(5, user.getLastName());
        statement.setString(6, user.getMiddleName());
        statement.setBoolean(7, user.isLocked());
        statement.setString(8, user.getRole().toString());
        statement.setString(9, user.getId());
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ?",
                getTableName(), COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ?",
                getTableName(), COLUMN_LOGIN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ?",
                getTableName(), COLUMN_EMAIL
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
